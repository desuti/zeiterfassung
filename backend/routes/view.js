var express = require('express');
var router = express.Router();
const rp = require('request-promise')
const getOptions = {
  method:'GET',
  uri:'http://localhost:5984/zeiten',
  json:true
}
/* GET home page. */
router.get('/', async function(req, res, next) {
  getOptions.uri+='/_design/times/_view/new-view'
  let a;
  a = await request(getOptions)
  res.send(a.rows)
});

async function request(options){
  return rp(options)
  .then(function(res){
    return res;
  })
}

module.exports = router;
