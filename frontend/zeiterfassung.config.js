angular.module('zeiterfassung').config(['$locationProvider', '$httpProvider', '$stateProvider', '$urlRouterProvider',
  function config ($urlServiceProvider, $httpProvider, $stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/view')

    $stateProvider.state('erfassen', {
      url: '/erfassen',
      component: 'erfassen',
      resolve: {
      }
    })
    $stateProvider.state('view', {
      url: '/view',
      component: 'view',
      resolve: {
      }
    })
}])
