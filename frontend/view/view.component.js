angular.module('zeiterfassung')
  .component('view',{
    templateUrl:'view/view.template.html',
    controller: viewController,
    bindings:{

    }
  })
  .service('viewService', viewService)

  function viewController($scope, viewService){
    $scope.test = 'hallo'
    console.log('test')

    console.log(viewService.getTimes())
  }

  function viewService($http){
    console.log('')
    var service = this
    service.getTimes = function(){
      return $http.get()
      .then(function (res){
        return res
      })
    }
  }
