var express = require('express');
var router = express.Router();

var view = require('./view');
/* GET home page. */
router.use('/view', view)

module.exports = router;
